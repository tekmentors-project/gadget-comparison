const _entity_name = 'cat_meta';
const _config = {
	apiKey: "AIzaSyDlrdzteenKnhKDIMMwlYicZsKunbjPXg4",
	authDomain: "gadgetx-1ed9d.firebaseapp.com",
	databaseURL: "https://gadgetx-1ed9d.firebaseio.com",
	projectId: "gadgetx-1ed9d",
	storageBucket: "gadgetx-1ed9d.appspot.com",
	messagingSenderId: "698373491548"
};

class CategoryDB
{
	constructor() {
		this._entity = _entity_name;

		if(!firebase.apps.length) {
			this._init_db();
		}
	}

	_init_db() {
		// Initialize Firebase
		firebase.initializeApp(_config);
	}

	_get_entity_name() {
		return _entity_name;
	}

	_get_schema() {
		let _schema = {
			"category_name" : "",
			"category_thumb" : "",
			"heading" : {}
		}

		return _schema;
	}

	getCategories() {
		return this._getCategoryData();
	}

	getCategoryDetails(category_id) {
		return this._getCategoryData(category_id);
	}

	_getCategoryData(category_id) {

		let end_point, ref;

		end_point = (!category_id) ? '/' + this._get_entity_name() : '/' + this._get_entity_name() + '/' + category_id;
		ref = firebase.database().ref(end_point);

		return Promise.resolve(true)
			.then(() => {
				return ref.once('value');
			})
			.then((snapshot) => {
				return snapshot.val();
			})
			.catch((error) => {
				console.log(error);
				alert(JSON.stringify(error));

				return false;
			})

	}

	saveCategory(cat_meta_data) {
		let ref = firebase.database().ref('/' + this._get_entity_name());

		return Promise.resolve(true)
			.then(() => {

				let schema = this._get_schema();
				let data_to_save = {};

				for(let _key in schema) {
					if(cat_meta_data[_key] == void(0)) {
						data_to_save[_key] = schema[_key];
						continue;
					}

					data_to_save[_key] = cat_meta_data[_key];
				}

				return ref.push(data_to_save);
			})
			.then((_result) => {
				console.log('Category Data Saved Successfully.' + JSON.stringify(_result));
			})
			.catch((error) => {
				console.log(error);
				alert(JSON.stringify(error));

				return false;
			})
	}

	updateCategory(cat_meta_data) {
		let ref = firebase.database().ref(_config.databaseURL + '/' + this._get_entity_name() + '/' + cat_meta_data.id);

		return Promise.resolve(true)
			.then(() => {
				let schema = this._get_schema();
				let data_to_save = {};

				for(let _key in schema) {
					if(cat_meta_data[key] == void(0)) {
						continue;
					}

					data_to_save[key] = cat_meta_data[key];
				}

				return ref.update(data_to_save);
			})
			.then((_result) => {
				console.log('Category Data Updated Successfully.' + JSON.stringify(_result));
			})
			.catch((error) => {
				console.log(error);
				alert(JSON.stringify(error));

				return false;
			})
	}
}