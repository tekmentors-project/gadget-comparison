import {GxNavigationsModule} from './../header.js';

export class GxHomePage {
	constructor() {
		window.onload = function() {
			GxNavigationsModule.init();
		}
	}
}

let GxHomePageModule = new GxHomePage();

export {GxHomePageModule};