let util, category_name;

util = new Utility();
category_name = (util.getQueryStringValue('cat')) ? util.getQueryStringValue('cat') : 'mobile';

const config = {
    apiKey: "AIzaSyDlrdzteenKnhKDIMMwlYicZsKunbjPXg4",
    authDomain: "gadgetx-1ed9d.firebaseapp.com",
    databaseURL: "https://gadgetx-1ed9d.firebaseio.com",
    projectId: "gadgetx-1ed9d",
    storageBucket: "gadgetx-1ed9d.appspot.com",
    messagingSenderId: "698373491548"
};

firebase.initializeApp(config);

function getDataByUserId() {
    return new Promise((resolve, reject) => {
        firebase.database().ref(`products`)
            .once("value", function (snapshot) {
                console.log("----------" + snapshot.val());
                resolve(snapshot.val());
            }, function (errorObject) {
                reject(errorObject.message)
            })
    })

}

window.onload = function () {
    function _prepareHTML(_categories) {
        let category_thumbs_template = '';
        let product_submenu_template = '';
        let compare_submenu_links = '';

        for (let element in _categories) {
            category_thumbs_template += `<div class='col-md-6 col-lg-3'>
                        <div class='card mb-4 view overlay mx-auto text-center' id='inner_card'>
                            <a href='#'>
                                <div class='card-img-overlay text-white d-flex align-items-center bgcolor_card'>
                                    <h4 class='card-title text-center w-100' id='item-title'>${_categories[element]['category_name']}</h4>
                                </div>
                                <div style='background:url(${_categories[element]['category_thumb']}); background-size: 100% 100%; background-repeat:no-repeat; height:250px;' aria-label='${_categories[element]['category_name']} '></div>
                            </a>
                        </div>
                    </div>`;

            product_submenu_template += `<a class='dropdown-item' href='products.html?cat=${encodeURI((_categories[element]['category_name']).toLowerCase())}'>
                        ${_categories[element]['category_name']}
                    </a>`;

            compare_submenu_links += `<a class='dropdown-item' href='comparison.html?cat=${encodeURI((_categories[element]['category_name']).toLowerCase())}'>
                                ${_categories[element]['category_name']}
                            </a>`;
        };

        $("#card_row").html(category_thumbs_template);
        $("#product_link").html(product_submenu_template);
        $("#footer_link").html(product_submenu_template);
        $("#category_link").html(compare_submenu_links);
        $("#category_footer").html(compare_submenu_links);
    }

    $("#filter").click(function () {
        $(".product-filter").toggle();
        $(".container").toggle();
        $("#filterPressed").toggle();
    });

    function searchMobiles() {
    
        var mobiles = [];
        var category_id;
      

         var productDetails = '';
        Promise.resolve()
            .then(() => {
                return _getCategory(category_name);
            })
            .then((_category) => {
                category_id = _category.id

                return getDataByUserId();
            })
            .then((data) => {
                let products = [];

                for (let _p_key in data) {
                    if (data[_p_key]['category'] == category_id) {
                        products.push(data[_p_key]);

                        mobiles.push({
                            "value": data[_p_key]['name'],
                            "label": data[_p_key]['name'],
                            "productId": _p_key
                        });
                    }
                }

                productDetails = {
                    Products: products
                };
                console.log("the mobile data is" + mobiles);
                             
                    for (let product_property in productDetails.Products) {
                        var template = '';
                        console.log("the product property is"+product_property);
                        template+= `<div class="bg-light text-dark col-12 border rounded mt-2" id="productSection">
                                        <div class="row p-4">
                                            <div class="col-2 col-md-2" id="productImage">
                                                <img class='mobile-image' src='${productDetails.Products[product_property].thumb}'>
                                            </div>
                                            <div class='col-10 col-md-10'>
                                                <div class='row'>
                                                    <div class='col-12 col-md-6'>
                                                        <p class='text-left'>
                                                            <b>${productDetails.Products[product_property].name}</b>
                                                        </p>
                                                    </div>
                                                    <div class='col-6 col-md-6 text-right'>
                                                        <b>${productDetails.Products[product_property].price}</b>
                                                    </div>
                                                    <div class='col-6'></div>
                                                </div>
                                                <div class='row'>`;                                         
                                                    var headingCounter = 1;
                                                    
                                                    for (let _heading_index in productDetails.Products[product_property]['heading']) {
                                                        debugger;
                                                        if (!productDetails.Products[product_property]['heading'].hasOwnProperty(_heading_index)) {
                                                            continue;
                                                        }

                                                        if(headingCounter >=5 ) {
                                                            break;
                                                        }
                                                        console.log('Heading Index : ' + _heading_index);
                                                      
                                                        
                                                        template+= 
                                                        "<div class='col-md-3 col-6 text-left text-capitalize'> <b>"+_heading_index+"</b><br><br>"+
                                                        "<span class='product-details'>";
                                                        
                                                        for (let _heading_key in productDetails.Products[product_property]['heading'][_heading_index]) {
                                                            if (!productDetails.Products[product_property]['heading'][_heading_index].hasOwnProperty(_heading_key)) {
                                                                continue;
                                                            }
                                                            console.log('Heading Key : ' + _heading_key);
                                                            console.log('HEading value: ' + productDetails.Products[product_property]['heading'][_heading_index][_heading_key]);           
                                                            template+=
                                                                 productDetails.Products[product_property]['heading'][_heading_index][_heading_key] + "<br>";                                           
                                                        }
                                                       
                                                        template+= "</span></div>";

                                                        headingCounter += 1;

                                                    }
                                                    
                                    template+=`</div>
                                            </div>
                                        </div>
                                    </div>`;
                        console.log("the template is"+template);
                    document.getElementById("content").innerHTML += template;
                    }
                   
                //}
            });
    }

    Promise.resolve(true)
        .then(() => {
            return _getCategories();
        })
        .then((_categories) => {
            _prepareHTML(_categories);
            return true;
        })
        .then(() => {
            searchMobiles();

            return true;
        })


    function _getCategories() {
        let category_db = new CategoryDB();

        return category_db.getCategories();
    }

    function _getCategory(cat_name) {
        return _getCategories()
            .then((_categories) => {
                let cat_data;

                for (let cat_id in _categories) {
                    if ((_categories[cat_id]['category_name']).toLowerCase() != category_name) {
                        continue;
                    }

                    cat_data = _categories[cat_id];
                    cat_data['id'] = cat_id;
                }

                return cat_data;
            });
    }

}