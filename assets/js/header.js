class GxNavigations {
    constructor() {

    }

    _getCategories() {
        let category_db = new CategoryDB();
    
        return category_db.getCategories();
    }
	
    _prepareRenderHTML(_categories) {
        let category_thumbs_template = '';
        let product_submenu_template = '';
        let compare_submenu_links = '';
        
        for(let element in _categories) {
            category_thumbs_template += `<div class='col-md-6 col-lg-3'>
                        <div class='card mb-4 view overlay mx-auto text-center' id='inner_card'>
                            <a href='/products.html?cat=${encodeURI((_categories[element]['category_name']).toLowerCase())}'>
                                <div class='card-img-overlay text-white d-flex align-items-center bgcolor_card'>
                                    <h4 class='card-title text-center w-100' id='item-title'>${_categories[element]['category_name']}</h4>
                                </div>
                                <div style='background:url(${_categories[element]['category_thumb']}); background-size: 100% 100%; background-repeat:no-repeat; height:250px;' aria-label='${_categories[element]['category_name']} '></div>
                            </a>
                        </div>
                    </div>`;
                
            product_submenu_template += `<a class='dropdown-item' href='./products.html?cat=${encodeURI((_categories[element]['category_name']).toLowerCase())}'>
                        ${_categories[element]['category_name']}
                    </a>`;

            compare_submenu_links += `<a class='dropdown-item' href='./comparison.html?cat=${encodeURI((_categories[element]['category_name']).toLowerCase())}'>
                                ${_categories[element]['category_name']}
                            </a>`;
        };
        
        $("#card_row").html(category_thumbs_template);
        $("#product_link").html(product_submenu_template);
        $("#footer_link").html(product_submenu_template);
        $("#category_link").html(compare_submenu_links);
        $("#category_footer").html(compare_submenu_links);
    }

    init() {
        return Promise.resolve(true)
            .then(() => {
                return this._getCategories();
            })
            .then((_categories) => {
                this._prepareRenderHTML(_categories);

                return true;
            });
    }
}

let GxNavigationsModule = new GxNavigations();

export {GxNavigationsModule};