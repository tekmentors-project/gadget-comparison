import {GxNavigationsModule} from './../header.js';


let util = new Utility();
const config = {
    apiKey: "AIzaSyDlrdzteenKnhKDIMMwlYicZsKunbjPXg4",
    authDomain: "gadgetx-1ed9d.firebaseapp.com",
    databaseURL: "https://gadgetx-1ed9d.firebaseio.com",
    projectId: "gadgetx-1ed9d",
    storageBucket: "gadgetx-1ed9d.appspot.com",
    messagingSenderId: "698373491548"
};

class GxComparison {
    constructor() {
        this.category_name = (util.getQueryStringValue('cat')) ? util.getQueryStringValue('cat') : 'mobile';
        firebase.initializeApp(config);

        window.onload = function() {
            return Promise.resolve(true)
                .then(() => {
                    return GxNavigationsModule.init();
                })
                .then(() => {
                    return this._populateSpecifications();
                })
                .then(() => {
                    return this._initClickHandlers();
                })
        }.bind(this);
    }

    _getProducts() {
        return new Promise((resolve, reject) => {
            firebase.database().ref(`products`)
                .once("value", function (snapshot) {
                    resolve(snapshot.val());
                }, function (errorObject) {
                    reject(errorObject.message)
                })
        })
    }

    _getCategories() {
        let category_db = new CategoryDB();

        return category_db.getCategories();
    }

    _getCategory(cat_name) {
        return this._getCategories()
            .then((_categories) => {
                let cat_data;

                for (let cat_id in _categories) {
                    if ((_categories[cat_id]['category_name']).toLowerCase() != cat_name) {
                        continue;
                    }

                    cat_data = _categories[cat_id];
                    cat_data['id'] = cat_id;
                }

                return cat_data;
            });
    }

    _initClickHandlers() {
        $('#summary').click(function () {
            $('.table-summary').toggle();
        });
        $('#general').click(function () {
            $('.table-general').toggle();
        });
        $('#design').click(function () {
            $('.table-design').toggle();
        });
        $('#display').click(function () {
            $('.table-display').toggle();
        });
    }

    _initializeAutocompletes() {
        var mobiles = [];
        var category_id;

        var productDetails = '';
        Promise.resolve()
            .then(() => {
                return this._getCategory(this.category_name);
            })
            .then((_category) => {
                category_id = _category.id

                return this._getProducts();
            })
            .then((data) => {
                let products = [];
                for (let _p_key in data) {
                    if (data[_p_key]['category'] == category_id) {
                        products.push(data[_p_key]);

                        mobiles.push({
                            "value": data[_p_key]['name'],
                            "label": data[_p_key]['name'],
                            "productId": _p_key
                        });
                    }
                }

                productDetails = {
                    Products: products
                };

                $("#searchCompare1, #searchCompare2, #searchCompare3").autocomplete({
                    source: (request, response) => {

                        Promise.resolve(true)
                            .then(() => {
                                let p_1_exclude, p_2_exclude, p_3_exclude;

                                p_1_exclude = jQuery("#searchCompare1").attr('product_id');
                                p_2_exclude = jQuery("#searchCompare2").attr('product_id');
                                p_3_exclude = jQuery("#searchCompare3").attr('product_id');

                                var filterredMobile = [];
                                for (let key in mobiles) {
                                    if (mobiles[key].productId == p_1_exclude
                                        || mobiles[key].productId == p_2_exclude
                                        || mobiles[key].productId == p_3_exclude) {
                                        continue;
                                    }

                                    if ((mobiles[key].label).toLowerCase().indexOf(request.term) >= 0) {
                                        filterredMobile.push(mobiles[key]);
                                    }
                                }

                                return filterredMobile;
                            })
                            .then((_mobiles) => {
                                response(_mobiles);
                            });
                    },
                    minLength: 2,
                    select: function (event, ui) {
                        jQuery(this).attr('product_id', ui.item.productId);

                        var index = productDetails.Products.findIndex(element => element.name == ui.item.label);
                        let template = '';
                        for (let product_property in productDetails.Products[index]) {
                            if (!productDetails.Products[index].hasOwnProperty(product_property)) {
                                continue;
                            }

                            if (product_property == 'heading') {
                                for (let _heading_index in productDetails.Products[index][product_property]) {
                                    if (!productDetails.Products[index][product_property].hasOwnProperty(_heading_index)) {
                                        continue;
                                    }

                                    for (let _heading_key in productDetails.Products[index][product_property][_heading_index]) {
                                        if (!productDetails.Products[index][product_property][_heading_index].hasOwnProperty(_heading_key)) {
                                            continue;
                                        }

                                        let column_number = jQuery(this).attr('col-number');
                                        let columnHeaderDataClass = "jump_data_" + column_number;

                                        template = "<div class='row justify-content-center align-items-center pt-3'>" +
                                                "<img src='" + productDetails.Products[index].thumb + "' width='150'>" +
                                            "</div>"+
                                            "<div class='row justify-content-center align-items-center'>"+
                                                 productDetails.Products[index].name+
                                                "<br>"+productDetails.Products[index].price+ "<br>" +
                                            "</div>" +
                                            "<div class='row justify-content-center align-items-center'>" +
                                                "<button class='btn btn-sm btn-warning'>Buy Now</button>"+
                                            "</div>";

                                        jQuery("#" + columnHeaderDataClass).html("");
                                        jQuery("#" + columnHeaderDataClass).html(template);

                                        let columnDataClass = ("heading_" + _heading_index + "_" + _heading_key + "_data_" + column_number).toLowerCase().replace(/[^A-Z0-9]+/ig, "_");
                                        template = productDetails.Products[index][product_property][_heading_index][_heading_key];
                                        jQuery("#" + columnDataClass).html("");
                                        jQuery("#" + columnDataClass).html(template);
                                    }
                                }
                            }
                        }
                        return true;
                    }
                })
                .data('autocomplete')._renderItem = function (ul, item) {
                    return $("<li></li>")
                        .data('item.autocomplete', item)
                        .append("<a product_id='" + item.productId + "'>" + item.label + "</a>")
                        .appendTo(ul);
                };
            });
    }

    _populateSpecifications() {
        var productDetails = '';
        var category_id;
        
        return Promise.resolve()
            .then(() => {
                return this._getCategory(this.category_name);
            })
            .then((_category) => {
                category_id = _category.id

                return this._getProducts();
            })
            .then((data) => {
                let products = [];
                for (let _p_key in data) {
                    if (data[_p_key]['category'] == category_id) {
                        products.push(data[_p_key]);
                    }
                }

                productDetails = {
                    Products: products
                };

                let template = '';
                let headerColumn1DataClass = "jump_data_1";
                let headerColumn2DataClass = "jump_data_2";
                let headerColumn3DataClass = "jump_data_3";
                template += "<tr><th></th><td id='" + headerColumn1DataClass + "'></td>" +
                    "<td id='" + headerColumn2DataClass + "'></td>" +
                    "<td id='" + headerColumn3DataClass + "'></td></tr>";


                for (let product_index in productDetails.Products) {
                    if (!productDetails.Products.hasOwnProperty(product_index)) {
                        continue;
                    }

                    for (let product_property in productDetails.Products[product_index]) {
                        if (!productDetails.Products[product_index].hasOwnProperty(product_property)) {
                            continue;
                        }

                        if (product_property == 'heading') {
                            for (let _heading_index in productDetails.Products[product_index][product_property]) {
                                if (!productDetails.Products[product_index][product_property].hasOwnProperty(_heading_index)) {
                                    continue;
                                }

                                let headingClass = ("heading_" + _heading_index.toLowerCase()).replace(/[^A-Z0-9]+/ig, "_");
                                let tableBodyClass = ("table_heading_" + _heading_index.toLowerCase()).replace(/[^A-Z0-9]+/ig, "_");
                                template += "<tr id='" + "heading" + headingClass + "-row" + "' class='bg-light'><th id='" + "heading_" + headingClass + "' class='p-4'>" + _heading_index + "</th>" +
                                    "<td class='width-33'></td>" +
                                    "<td class='width-33'></td>" +
                                    "<td class='width-33'></td>" +
                                    "</tr><tbody class='" + tableBodyClass + "'>";


                                for (let _heading_key in productDetails.Products[product_index][product_property][_heading_index]) {
                                    if (!productDetails.Products[product_index][product_property][_heading_index].hasOwnProperty(_heading_key)) {
                                        continue;
                                    }

                                    let tableRowClass = (("heading_" + _heading_index.toLowerCase() + "_" + _heading_key.toLowerCase() + "_row").toLowerCase()).replace(/[^A-Z0-9]+/ig, "_");
                                    let firstColumnTableDataClass = (("heading_" + _heading_index.toLowerCase() + "_" + _heading_key.toLowerCase() + "_data_" + jQuery("#searchCompare1").attr('col-number')).toLowerCase()).replace(/[^A-Z0-9]+/ig, "_");
                                    let secondColumnTableDataClass = (("heading_" + _heading_index.toLowerCase() + "_" + _heading_key.toLowerCase() + "_data_" + jQuery("#searchCompare2").attr('col-number')).toLowerCase()).replace(/[^A-Z0-9]+/ig, "_");
                                    let thirdColumnTableDataClass = (("heading_" + _heading_index.toLowerCase() + "_" + _heading_key.toLowerCase() + "_data_" + jQuery("#searchCompare3").attr('col-number')).toLowerCase()).replace(/[^A-Z0-9]+/ig, "_");

                                    template += "<tr id='" + tableRowClass + "'><td class='p-4'>" + _heading_key + "</td>" +
                                        "<td class='p-4 width-33' id = '" + firstColumnTableDataClass + "' ></td>" +
                                        "<td class='p-4 width-33' id='" + secondColumnTableDataClass + "'></td>" +
                                        "<td class='p-4 width-33' id='" + thirdColumnTableDataClass + "'></td>" +
                                        "</tr>";
                                }
                                template += "</tbody>";
                            }
                        }
                    }
                    break;
                }

                document.getElementById("main-table").innerHTML += template;
            })
            .then(() => {
                this._initializeAutocompletes();
                return true;
            })
    }
}

let GxComparisonModule = new GxComparison();

export {GxComparisonModule};

// function toggleHeadings(){
//     productDetails.Products.heading.forEach(function (element) {
//         let headingClass = "heading"+("heading_" +element.toLowerCase()).replace(/[^A-Z0-9]+/ig, "_")+"-row";
//         let tableBodyClass = ("table_heading_" + element.toLowerCase()).replace(/[^A-Z0-9]+/ig, "_");

//         $('#headingClass').click(function () {
//             $('.tableBodyClass').toggle();
//         });
//     });
// }