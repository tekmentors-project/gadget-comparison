window.onload = function() {
	let product_db = new ProductDB();

	return Promise.resolve(true)
		.then(() => {
			return product_db.getProducts();
		})
		.then((_products) => {
			populateListing(_products);
		});
}

function populateListing(_products) {

	let final_html = "";

	let _row_number = 1;
	for(let _key in _products) {
		final_html += `<tr product_id="${_key}">
	        <th scope="row">
	        	<a href="./../products/details.html?product_id=${_key}">
	        		${_row_number}
	        	</a>
	       	</th>
	        <td>${_products[_key]['name']}</td>
	        <td>
	            <img class="rounded img-thumbnail" src="${_products[_key]['thumb']}">
	        </td>
	        <td>${(new Date()).toLocaleString()}</td>
	    </tr>`;

	    _row_number += 1;
	}

	jQuery("tbody").html("");
	jQuery("tbody").html(final_html);
}

jQuery(document).on('click', "#addNewProduct", (event) => {
	//history.pushState(null, null, './../../admin/categories/add-new.html ' + "Add New Category");
})