window.onload = function() {
	let cat_db = new CategoryDB();

	return Promise.resolve(true)
		.then(() => {
			return cat_db.getCategories();
		})
		.then((_categories) => {
			populateListing(_categories);
		});
}

function populateListing(_categories) {

	let final_html = "";

	let _row_number = 1;
	for(let _key in _categories) {
		final_html += `<tr category_id="${_key}">
	        <th scope="row">
	        	<a href="./../categories/details.html?category_id=${_key}">
	        		${_row_number}
	        	</a>
			</th>
	        <td>${_categories[_key]['category_name']}</td>
	        <td>
	            <img class="rounded img-thumbnail" src="${_categories[_key]['category_thumb']}">
	        </td>
	        <td>${(_categories[_key]['created_at']) ? (new Date(parseInt(_categories[_key]['created_at']))).toLocaleString() : ''}</td>
	    </tr>`;

	    _row_number += 1;
	}

	jQuery("tbody").html("");
	jQuery("tbody").html(final_html);
}

jQuery(document).on("click", "#addNewCategory", function() {
	window.location.href = "/admin/categories/add-new.html"
})
