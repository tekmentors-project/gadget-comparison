window.onload = function(event) {
	let util, product_id;

	util = new Utility();
	product_id  = util.getQueryStringValue('product_id');

	function populateDataOnPage()
	{
		let product;

		return Promise.resolve(true)
			.then(() => {
				return _getCategoryList();
			})
			.then((_categories) => {
				populateCategoriesList(_categories);

				return _getProductDetails(product_id)
			})
			.then((product_details) => {

				let category_id = product_details.category;
				
				product = product_details;

				return getCategoryFields(category_id);
			})
			.then((_category_field) => {
				populateDynamicForm(_category_field, product);

				return true;
			})
			.catch((error) => {
				console.error(error);

				throw error;
			});
	}

	function _getProductDetails(product_id) {
		return Promise.resolve(true)
			.then(() => {
				let product_db = new ProductDB();

				return product_db.getProductDetails(product_id);
			})
	}

	function _getCategoryList() {
		return Promise.resolve(true)
			.then(() => {
				let cat_db = new CategoryDB();

				return cat_db.getCategories();
			});
	}

	function _getCategoryDetails(category_id) {
		return Promise.resolve(true)
			.then(() => {
				let cat_db = new CategoryDB();

				return cat_db.getCategoryDetails(category_id);
			});
	}

	function populateCategoriesList(categories) {
		let select_options = `<option value="" class="product_type_selected">Select Product Type</option>`;

		for(let key in categories) {
			select_options += `<option value="${key}" class="product_type_selected">${categories[key]['category_name']}</option>`;
		}

		jQuery("#categories_list").html("");
		jQuery("#categories_list").html(select_options);
	}

	function getCategoryFields(category_id) {
		return _getCategoryDetails(category_id)
			.then((_category_dtails) => {
				return (_category_dtails['heading']) ? _category_dtails['heading'] : {};
			})
	}

	function populateDynamicForm(_schema, product_details) {
		let template = ``;

		jQuery('#productName').val(product_details.name);
		jQuery('#productThumb').val(product_details.thumb);
		jQuery('#productBrand').val(product_details.brand);
		jQuery('#productPrice').val((product_details.price) ? product_details.price : "");
		jQuery(`#categories_list option[value='${product_details.category}']`).attr('selected', 'selected');

		for(let _heading in _schema) {

			let _heading_name = _heading.toLowerCase();
			template += `<div class="col-12 mb-3 p-0">
	            			<div class="card">
	                			<div class="card-body">
	                				<h5 class="card-title">${_heading}</h5><hr class=""/>`;
	                
	        for(let _field_index in _schema[_heading]['field']) {
	        	template += `<div class="form-group">
				                <label for="heading_${_heading}_field_${_field_index}_name">${_schema[_heading]['field'][_field_index]['name']}</label>`

				if(_schema[_heading]['field'][_field_index]['type'] === "text") {
					let _input_name = (_schema[_heading]['field'][_field_index]['name']).toLowerCase();
				    template += `<input type="text" class="form-control" id="heading_${_heading}_field_${_field_index}_name" name="heading[${_heading_name}][${_input_name}]" placeholder="${_schema[_heading]['field'][_field_index]['name']}" value="${(product_details['heading'][_heading_name] && product_details['heading'][_heading_name][_input_name]) ? product_details['heading'][_heading_name][_input_name] : ''}">`;
				}

				if(_schema[_heading]['field'][_field_index]['type'] === "select") {
					let _select_name = (_schema[_heading]['field'][_field_index]['name']).toLowerCase();
					template += `<select class="form-control" id="${_heading + '_' +_field_index}" name="heading[${_heading_name}][${_select_name}]">
									<option value="" class="text-capitalize product_${(_schema[_heading]['field'][_field_index]['name']).replace(/ /g, "_")}_selected">Select ${_schema[_heading]['field'][_field_index]['name']}</option>`;


					for(let _option_index in _schema[_heading]['field'][_field_index]['option']) {
						template += `<option value="${_schema[_heading]['field'][_field_index]['option'][_option_index]['name']}" class="text-capitalize product_${(_schema[_heading]['field'][_field_index]['option'][_option_index]['name']).replace(/ /g, "_")}_selected" ${(product_details['heading'][_heading_name] && _schema[_heading]['field'][_field_index]['option'][_option_index]['name'] == product_details['heading'][_heading_name][_select_name]) ? 'selected="selected"' : ""}>${_schema[_heading]['field'][_field_index]['option'][_option_index]['name'] + " " + _schema[_heading]['field'][_field_index]['option'][_option_index]['unit']}</option>`;
					}

					template += `</select>`;
				}

				template += `</div>`;
	        }
	        

	        template += `</div>
	            </div>
	        </div>`;
		}

		jQuery("#dynamicFeildWrapper").html("");
		jQuery("#dynamicFeildWrapper").html(template);
	}


	populateDataOnPage();

	jQuery(document).ready(function() {
		function handleProductUpdate(event) {
			event.preventDefault();
		
			return Promise.resolve(true)
				.then(() => {
					let product_db = new ProductDB();

					let form_data_json = convertFormDataToObject(jQuery('#createProductForm'));

					return product_db.updateProduct(product_id, form_data_json);
				})
				.then((results) => {
					let utilityObj = new Utility();

					alert("Product Saved Successfully.");
					utilityObj.redirect('products/listing.html');

					return true;
				});
		}

		jQuery(document).on('click', "#updateProduct", handleProductUpdate)
	})
}