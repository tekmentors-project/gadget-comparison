var extractFieldNames = function(fieldName, expression, keepFirstElement) 
{
    expression = expression || /([^\]\[]+)/g;
    keepFirstElement = keepFirstElement || false;
    
    var elements = [];

    while((searchResult = expression.exec(fieldName)))
    {
        elements.push(searchResult[0]);
    }
    
    if (!keepFirstElement && elements.length > 0) elements.shift();
    
    return elements;
}

var attachProperties = function(target, properties, value) 
{
    var currentTarget = target;
    var propertiesNum = properties.length;
    var lastIndex = propertiesNum - 1;

    for (var i = 0; i < propertiesNum; ++i) 
    {
        currentProperty = properties[i];
        
        if (currentTarget[currentProperty] === undefined) {
            currentTarget[currentProperty] = (i === lastIndex) ? value : {};
        }

        currentTarget = currentTarget[currentProperty];
    }
}

var convertFormDataToObject = function(form)
{
    var currentField = null;
    var currentProperties = null;
    
    // result of this function
    var data = {};

    // get array of fields that exist in this form
    var fields = form.serializeArray();

    for (var i = 0; i < fields.length; ++i) {
      currentField = fields[i];                  
      // extract field names
      currentProperties = extractFieldNames(currentField.name, null, true);                 
      // add new fields to our data object
      attachProperties(data, currentProperties, currentField.value);
    }
    
    return data;
}