class Utility {
	constructor() {

	}

	redirect(path) {
		window.location.href = "./../" + path;
	}

	getQueryStringValue(key) {  
  		return decodeURIComponent(
  			window.location.search.replace(new RegExp("^(?:.*[&\\?]" + encodeURIComponent(key).replace(/[\.\+\*]/g, "\\$&") + "(?:\\=([^&]*))?)?.*$", "i"), "$1")
  		);
	}
}