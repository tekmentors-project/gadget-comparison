class ProductDB
{
	constructor() {
		this._entity = 'products';
		this._config = {
			apiKey: "AIzaSyDlrdzteenKnhKDIMMwlYicZsKunbjPXg4",
			authDomain: "gadgetx-1ed9d.firebaseapp.com",
			databaseURL: "https://gadgetx-1ed9d.firebaseio.com",
			projectId: "gadgetx-1ed9d",
			storageBucket: "gadgetx-1ed9d.appspot.com",
			messagingSenderId: "698373491548"
		};

		if(!firebase.apps.length) {
			this._init_db();
		}
	}

	_init_db() {
		// Initialize Firebase
		firebase.initializeApp(this._config);
	}

	_get_entity_name() {
		return this._entity;
	}

	_getProductData(product_id) {
		let end_point, ref;

		end_point = (!product_id) ? '/' + this._get_entity_name() : '/' + this._get_entity_name() + '/' + product_id;
		ref = firebase.database().ref(end_point);

		return Promise.resolve(true)
			.then(() => {
				return ref.once('value');
			})
			.then((snapshot) => {
				return snapshot.val();
			})
			.catch((error) => {
				console.error(error);
				throw error;
			})

	}

	getProducts() {
		return this._getProductData();
	}

	getProductDetails(product_id) {
		return this._getProductData(product_id);
	}

	saveProduct(product_data) {
		let ref = firebase.database().ref('/' + this._get_entity_name());

		return Promise.resolve(true)
			.then(() => {
				product_data["created_at"] = Date.now();
				product_data["updated_at"] = Date.now();
				
				return ref.push(product_data);
			})
			.catch((error) => {
				console.error(error);
				
				throw error;
			})
	}

	updateProduct(product_id, data_to_update) {
		let ref = firebase.database().ref('/' + this._get_entity_name() + '/' + product_id);

		return Promise.resolve(true)
			.then(() => {
				data_to_update["updated_at"] = Date.now();
				
				return ref.update(data_to_update);
			})
			.catch((error) => {
				console.error(error);

				throw error;
			})
	}
}