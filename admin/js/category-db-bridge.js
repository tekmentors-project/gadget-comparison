class CategoryDB
{
	constructor() {
		this._entity = 'cat_meta';
		this._config = {
			apiKey: "AIzaSyDlrdzteenKnhKDIMMwlYicZsKunbjPXg4",
			authDomain: "gadgetx-1ed9d.firebaseapp.com",
			databaseURL: "https://gadgetx-1ed9d.firebaseio.com",
			projectId: "gadgetx-1ed9d",
			storageBucket: "gadgetx-1ed9d.appspot.com",
			messagingSenderId: "698373491548"
		};

		if(!firebase.apps.length) {
			this._init_db();
		}
	}

	_init_db() {
		// Initialize Firebase
		firebase.initializeApp(this._config);
	}

	_get_entity_name() {
		return this._entity;
	}

	_get_schema() {
		let _schema = {
			"category_name" : "",
			"category_thumb" : "",
			"heading" : {},
			"created_at" : Date.now()
		}

		return _schema;
	}

	getCategories() {
		return this._getCategoryData();
	}

	getCategoryDetails(category_id) {
		return this._getCategoryData(category_id);
	}

	_getCategoryData(category_id) {

		let end_point, ref;

		end_point = (!category_id) ? '/' + this._get_entity_name() : '/' + this._get_entity_name() + '/' + category_id;
		ref = firebase.database().ref(end_point);

		return Promise.resolve(true)
			.then(() => {
				return ref.once('value');
			})
			.then((snapshot) => {
				return snapshot.val();
			})
			.catch((error) => {
				console.error(error);
				throw error;
			})

	}

	saveCategory(cat_meta_data) {
		let ref = firebase.database().ref('/' + this._get_entity_name());

		return Promise.resolve(true)
			.then(() => {

				let schema = this._get_schema();
				let data_to_save = {};

				for(let _key in schema) {
					if(cat_meta_data[_key] == void(0)) {
						data_to_save[_key] = schema[_key];
						continue;
					}

					data_to_save[_key] = cat_meta_data[_key];
				}

				return ref.push(data_to_save);
			})
			.catch((error) => {
				console.error(error);
				
				throw error;
			})
	}

	updateCategory(category_id, data_to_update) {
		let ref = firebase.database().ref('/' + this._get_entity_name() + '/' + category_id);

		return Promise.resolve(true)
			.then(() => {
				data_to_update["updated_at"] = Date.now();
				
				return ref.update(data_to_update);
			})
			.catch((error) => {
				console.error(error);
				
				throw error;
			})
	}
}