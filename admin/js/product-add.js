window.onload = function(event) {
	function showCategoryList()
	{
		return _getCategoryList()
			.then((_categories) => {
				populateCategoriesList(_categories);

				return true;
			})
			.catch((error) => {
				console.error(error);

				throw error;
			});
	}

	function _getCategoryList() {
		let cat_db = new CategoryDB();

		return cat_db.getCategories();
	}

	function _getCategoryDetails(category_id) {
		let cat_db = new CategoryDB();

		return cat_db.getCategoryDetails(category_id);
	}

	function populateCategoriesList(categories) {
		let select_options = `<option value="" class="product_type_selected">Select Product Type</option>`;

		for(let key in categories) {
			select_options += `<option value="${key}" class="product_type_selected">${categories[key]['category_name']}</option>`;
		}

		jQuery("#categories_list").html("");
		jQuery("#categories_list").html(select_options);
	}

	function getCategoryFields(category_id) {
		return _getCategoryDetails(category_id)
			.then((_category_dtails) => {
				return (_category_dtails['heading']) ? _category_dtails['heading'] : {};
			})
	}

	function populateDynamicForm(_schema) {
		let template = ``;

		for(let _heading in _schema) {

			let _heading_name = _heading.toLowerCase();
			template += `<div class="col-12 mb-3 p-0">
	            			<div class="card">
	                			<div class="card-body">
	                				<h5 class="card-title">${_heading}</h5><hr class=""/>`;
	                
	        for(let _field_index in _schema[_heading]['field']) {
	        	template += `<div class="form-group">
				                <label for="heading_${_heading}_field_${_field_index}_name">${_schema[_heading]['field'][_field_index]['name']}</label>`

				if(_schema[_heading]['field'][_field_index]['type'] === "text") {
					let _input_name = (_schema[_heading]['field'][_field_index]['name']).toLowerCase();
				    template += `<input type="text" class="form-control" id="heading_${_heading}_field_${_field_index}_name" name="heading[${_heading_name}][${_input_name}]" placeholder="${_schema[_heading]['field'][_field_index]['name']}">`;
				}

				if(_schema[_heading]['field'][_field_index]['type'] === "select") {
					let _select_name = (_schema[_heading]['field'][_field_index]['name']).toLowerCase();
					template += `<select class="form-control" id="${_heading + '_' +_field_index}" name="heading[${_heading_name}][${_select_name}]">
									<option value="" class="text-capitalize product_${(_schema[_heading]['field'][_field_index]['name']).replace(/ /g, "_")}_selected">Select ${_schema[_heading]['field'][_field_index]['name']}</option>`;


					for(let _option_index in _schema[_heading]['field'][_field_index]['option']) {
						template += `<option value="${_schema[_heading]['field'][_field_index]['option'][_option_index]['name']}" class="text-capitalize product_${(_schema[_heading]['field'][_field_index]['option'][_option_index]['name']).replace(/ /g, "_")}_selected">${_schema[_heading]['field'][_field_index]['option'][_option_index]['name'] + " " + _schema[_heading]['field'][_field_index]['option'][_option_index]['unit']}</option>`;
					}

					template += `</select>`;
				}

				template += `</div>`;
	        }
	        

	        template += `</div>
	            </div>
	        </div>`;
		}

		jQuery("#dynamicFeildWrapper").html("");
		jQuery("#dynamicFeildWrapper").html(template);
	}


	showCategoryList();

	jQuery(document).ready(function() {
		function handleCategoryListing(event) {
			let category_id = jQuery(this).val();

			return Promise.resolve(true)
				.then(() => {
					return getCategoryFields(category_id);
				})
				.then((_fields) => {
					populateDynamicForm(_fields);

					return false;
				})
		}

		function handleProductCreation(event) {
			event.preventDefault();
		
			return Promise.resolve(true)
				.then(() => {
					let product_db = new ProductDB();

					let form_data_json = convertFormDataToObject(jQuery('#createProductForm'));

					return product_db.saveProduct(form_data_json);
				})
				.then((results) => {
					let utilityObj = new Utility();

					alert("Product Saved Successfully.");

					utilityObj.redirect('products/listing.html');

					return true;
				});
		}

		jQuery(document).on('change', "#categories_list", handleCategoryListing);
		jQuery(document).on('click', "#createProduct", handleProductCreation)
	})
}