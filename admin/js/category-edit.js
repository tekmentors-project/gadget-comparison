jQuery(document).ready(function() {

	populateDataOnPage();

	let util, category_id;

	util = new Utility();
	category_id  = util.getQueryStringValue('category_id');

	function handleLogin(event) {
		// TODO :: Remove when going to integrate actual Login REST API
		event.preventDefault();
		window.location.href = "./dashboard/index.html";
		return true;
	}

	function populateDataOnPage() {
		return Promise.resolve(true)
			.then(() => {
				return _getCategoryData(category_id);
			})
			.then((_category_data) => {
				populateDyanamicForm(_category_data);

				return true;
			})
	}

	function _getCategoryData(category_id) {
		let category_db = new CategoryDB();

		return category_db.getCategoryDetails(category_id);
	}

	function populateDyanamicForm(_category_data) {
		let template = "";

		jQuery("#categoryName").val(_category_data.category_name);
		jQuery("#categoryThumb").val(_category_data.category_thumb);

		let _heading_index = 1;
		for(let _heading in _category_data['heading']) {
			template += `<div class="row mb-3">
                                <div class="col-11">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="heading_${_heading_index}">Heading</label>
                                                <input type="text" class="form-control" id="heading_${_heading_index}" name="heading[${_heading_index}][name]" placeholder="Like Performance." value="${_category_data['heading'][_heading]['name']}">
                                            </div>
                                            <div class="fieldsWrapper">`;

            for(let _field in _category_data['heading'][_heading]['field']) {
                template += `<div class="row mb-3">
                                <div class="col-11">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="form-group">
                                                <label for="heading_${_heading_index}_field_${_field}_name">Field Name</label>
                                                <input type="text" class="form-control" id="heading_${_heading_index}_field_${_field}_name" name="heading[${_heading_index}][field][${_field}][name]" placeholder="Storage." value="${_category_data['heading'][_heading]['field'][_field]['name']}">
                                            </div>
                                            <div class="form-group">
                                                <label for="heading_${_heading_index}_field_${_field}_type">Type</label>
                                                <select class="form-control select_field_type mb-3" id="heading_${_heading_index}_field_${_field}_type" name="heading[${_heading_index}][field][${_field}][type]">
                                                    <option value="">Select Type</option>
                                                    <option value="select" ${(_category_data['heading'][_heading]['field'][_field]['type'] == "select") ? 'selected="selected"' : ""}>Select Box</option>
                                                    <option value="text" ${(_category_data['heading'][_heading]['field'][_field]['type'] == "text") ? 'selected="selected"' : ""}>Text Box</option>
                                                </select>`;

                if(_category_data['heading'][_heading]['field'][_field]['type'] == "select") {
                	template += `<div class="selectWrapper">
                					`;

                    for(let _option in _category_data['heading'][_heading]['field'][_field]['option']) {
                    	template +=`<div class="row mb-3">
	                    				<div class="col-11">
			                                <div class="card">
			                                    <div class="card-body">
			                                        <div class="form-row">
			                                            <div class="col-md-4 mb-3">
			                                                <label for="heading_${_heading_index}_field_${_field}_option_${_option}_value">Value</label>
			                                                <input type="text" class="form-control" id="heading_${_heading_index}_field_${_field}_option_${_option}_value" name="heading[${_heading_index}][field][${_field}][option][${_option}][value]" placeholder="2" value="${_category_data['heading'][_heading]['field'][_field]['option'][_option]['value']}">
			                                            </div>
			                                            <div class="col-md-4 mb-3">
			                                                <label for="heading_${_heading_index}_field_${_field}_option_${_option}_name">Name</label>
			                                                <input type="text" class="form-control" id="heading_${_heading_index}_field_${_field}_option_${_option}_name" name="heading[${_heading_index}][field][${_field}][option][${_option}][name]" placeholder="2" value="${_category_data['heading'][_heading]['field'][_field]['option'][_option]['name']}">
			                                            </div>
			                                            <div class="col-md-4 mb-3">
			                                                <label for="heading_${_heading_index}_field_${_field}_option_${_option}_unit">Unit</label>
			                                                <input type="text" class="form-control" id="heading_${_heading_index}_field_${_field}_option_${_option}_unit" name="heading[${_heading_index}][field][${_field}][option][${_option}][unit]" placeholder="GB" value="${_category_data['heading'][_heading]['field'][_field]['option'][_option]['unit']}">
			                                            </div>
			                                        </div>
			                                    </div>
			                                </div>
			                            </div>
			                            <div class="col-1">
			                                <div class="bg-light">
			                                    <button ${(Object.keys(_category_data['heading'][_heading]['field'][_field]['option']).pop() == _option) ? 'style="display:block;"' : 'style="display:none;"'} id="addOptions_${_option}_heading_${_heading_index}_field_${_field}" heading_index="${_heading_index}" field_index="${_field}" option_index="${_option}" title="Add More Possible Value for Select Dropdown" class="btn btn-primary float-right rounded-circle">
			                                        <i class="fa fa-plus" aria-hidden="true"></i>
			                                    </button>
			                                </div>
			                                <br/>
			                                <br/>
			                                <div class="bg-light">
			                                    <button ${(Object.keys(_category_data['heading'][_heading]['field'][_field]['option']).pop() == _option) ? 'style="display:block;"' : 'style="display:none;"'} id="deleteOptions_${_option}_heading_${_heading_index}_field_${_field}" heading_index="${_heading_index}" field_index="${_field}" option_index="${_option}" title="Delete this section" class="btn btn-danger float-right rounded-circle">
				                                    <i class="fa fa-minus" aria-hidden="true"></i>
			                                    </button>
			                                </div>
			                            </div>
			                        </div>`;
                    }

					template += `</div>`;
                }
                                    
				template += `</div>
	                    </div>
	                </div>
	            </div>
	            <div class="col-1">
	                <div class="bg-light">
	                    <button style="${(Object.keys(_category_data['heading'][_heading]['field']).pop() == _field) ? "display:block;" : "display:none;"}" id="addField_${_field}_heading_${_heading_index}" heading_index="${_heading_index}" field_index="${_field}" title="Add More Field" class="btn btn-primary float-right rounded-circle">
	                        <i class="fa fa-plus" aria-hidden="true"></i>
	                    </button>
	                </div>
	                <br/>
                    <br/>
                    <div class="bg-light">
                        <button style="${(Object.keys(_category_data['heading'][_heading]['field']).pop() == _field) ? "display:block;" : "display:none;"}" id="deleteField_${_field}_heading_${_heading_index}" heading_index="${_heading_index}" field_index="${_field}" title="Delete this Section" class="btn btn-danger float-right rounded-circle">
                            <i class="fa fa-minus" aria-hidden="true"></i>
                        </button>
                    </div>
	            </div>
	        </div>`;
            }
                            
			template += `</div>
					</div>
                </div>
            </div>
            <div class="col-1">
                <div class="bg-light">
                    <button style="${(Object.keys(_category_data['heading']).pop() == _heading) ? "display:block;" : "display:none;"}" id="addHeading_${_heading_index}" heading_index="${_heading_index}" title="Add More Heading Section" class="btn btn-primary float-right rounded-circle">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                    </button>
                </div>
                <br/>
	            <br/>
	            <div class="bg-light">
	                <button style="${(Object.keys(_category_data['heading']).pop() == _heading) ? "display:block;" : "display:none;"}" id="deleteHeading_${_heading_index}" heading_index="${_heading_index}" title="Add More Heading Section" class="btn btn-danger float-right rounded-circle">
	                    <i class="fa fa-minus" aria-hidden="true"></i>
	                </button>
	            </div>
            </div>
        </div>`;

			_heading_index += 1;
		}

		jQuery(".headingWrapper").html("");
		jQuery(".headingWrapper").html(template);
	}

	function populateHeading(_heading_data, template) {
		for(let _field in _heading_data['field']) {
			populateFields(_heading_data[_field], template);
		}
	}

	function populateField(_field_data, template) {

		if(_field_data.type == 'text') {
			template += '';
		}
		
		if(_field_data.type == "select") {
			for(let _option_index in _field_data['option']) {
				template += '';
			}
		}
	}

	function handleAddHeading(event) {
		event.preventDefault();

		let recentIndexElement = jQuery(this).closest('.headingWrapper .row').last().find("[id^=addHeading_]");
		let heading_index = parseInt(recentIndexElement.attr("heading_index")) + 1;

		let template = `<div class="row mb-3">
	                        <div class="col-11">
	                            <div class="card">
	                                <div class="card-body">
	                                    <div class="form-group">
	                                        <label for="heading_${heading_index}">Heading</label>
	                                        <input type="text" class="form-control" id="heading_${heading_index}" name="heading[${heading_index}][name]" placeholder="Like Performance.">
	                                    </div>

	                                    <div class="fieldsWrapper">
	                                        <div class="row mb-3">
	                                            <div class="col-11">
	                                                <div class="card">
	                                                    <div class="card-body">
	                                                        <div class="form-group">
	                                                            <label for="heading_${heading_index}_field_1_name">Field Name</label>
	                                                            <input type="text" class="form-control" id="heading_${heading_index}_field_1_name" name="heading[${heading_index}][field][1][name]" placeholder="Storage.">
	                                                        </div>
	                                                        <div class="form-group">
	                                                            <label for="heading_${heading_index}_field_1_type">Type</label>
	                                                            <select class="form-control select_field_type mb-3" id="heading_${heading_index}_field_1_type" name="heading[${heading_index}][field][1][type]">
	                                                                <option value="">Select Type</option>
	                                                                <option value="select">Select Box</option>
	                                                                <option value="text">Text Box</option>
	                                                            </select>
	                                                            <div class="selectWrapper" style="display: none;">
	                                                                <div class="row mb-3">
	                                                                    <div class="col-11">
	                                                                        <div class="card">
	                                                                            <div class="card-body">
	                                                                                <div class="form-row">
	                                                                                    <div class="col-md-4 mb-3">
	                                                                                        <label for="heading_${heading_index}_field_1_option_1_value">Value</label>
	                                                                                        <input type="text" class="form-control" id="heading_${heading_index}_field_1_option_1_value" name="heading[${heading_index}][field][1][option][1][value]" placeholder="2">
	                                                                                    </div>
	                                                                                    <div class="col-md-4 mb-3">
	                                                                                        <label for="heading_${heading_index}_field_1_option_1_name">Name</label>
	                                                                                        <input type="text" class="form-control" id="heading_${heading_index}_field_1_option_1_name" name="heading[${heading_index}][field][1][option][1][name]" placeholder="2">
	                                                                                    </div>
	                                                                                    <div class="col-md-4 mb-3">
	                                                                                        <label for="heading_${heading_index}_field_1_option_1_unit">Unit</label>
	                                                                                        <input type="text" class="form-control" id="heading_${heading_index}_field_1_option_1_unit" name="heading[${heading_index}][field][1][option][1][unit]" placeholder="GB">
	                                                                                    </div>
	                                                                                </div>
	                                                                            </div>
	                                                                        </div>
	                                                                    </div>
	                                                                    <div class="col-1">
	                                                                        <div class="d-block bg-light">
	                                                                            <button id="addOptions_1_heading_${heading_index}_field_1" heading_index="${heading_index}" field_index="1" option_index="1" title="Add More Possible Value for Select Dropdown" class="btn btn-primary float-right rounded-circle">
	                                                                                <i class="fa fa-plus" aria-hidden="true"></i>
	                                                                            </button>
	                                                                        </div>
	                                                                    </div>
	                                                                </div>
	                                                            </div>
	                                                        </div>
	                                                    </div>
	                                                </div>
	                                            </div>
	                                            <div class="col-1">
	                                                <div class="d-block bg-light">
	                                                    <button id="addField_1_heading_${heading_index}" heading_index="${heading_index}" field_index="1" title="Add More Field" class="btn btn-primary float-right rounded-circle">
	                                                        <i class="fa fa-plus" aria-hidden="true"></i>
	                                                    </button>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>
	                        <div class="col-1">
	                            <div class="d-block bg-light">
	                                <button id="addHeading_${heading_index}" heading_index="${heading_index}" title="Add More Heading Section" class="btn btn-primary float-right rounded-circle">
	                                    <i class="fa fa-plus" aria-hidden="true"></i>
	                                </button>
	                            </div>
	                            <br/>
	                            <br/>
	                            <div class="d-block bg-light">
	                                <button id="deleteHeading_${heading_index}" heading_index="${heading_index}" title="Add More Heading Section" class="btn btn-danger float-right rounded-circle">
	                                    <i class="fa fa-minus" aria-hidden="true"></i>
	                                </button>
	                            </div>
	                        </div>
	                    </div>`;

		jQuery("#addHeading_" + (heading_index - 1)).hide();
        jQuery("#deleteHeading_" + (heading_index - 1)).hide();
        jQuery(this).closest('.headingWrapper').append(template);
	}

	function handleAddField(event) {
		event.preventDefault();

		let recentIndexElement = jQuery(this).closest('.fieldsWrapper .row').last().find("[id^=addField_]");
		let heading_index = recentIndexElement.attr("heading_index");
		let field_index = parseInt(recentIndexElement.attr("field_index")) + 1;
		let template = `<div class="row mb-3">
                            <div class="col-11">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-group">
                                            <label for="heading_${heading_index}_field_${field_index}_name">Field Name</label>
                                            <input type="text" class="form-control" id="heading_${heading_index}_field_${field_index}_name" name="heading[${heading_index}][field][${field_index}][name]" placeholder="Storage.">
                                        </div>
                                        <div class="form-group">
                                            <label for="heading_${heading_index}_field_${field_index}_type">Type</label>
                                            <select class="form-control select_field_type mb-3" id="heading_${heading_index}_field_${field_index}_type" name="heading[${heading_index}][field][${field_index}][type]">
                                                <option value="">Select Type</option>
                                                <option value="select">Select Box</option>
                                                <option value="text">Text Box</option>
                                            </select>
                                            <div class="selectWrapper" style="display: none;">
                                                <div class="row mb-3">
                                                    <div class="col-11">
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <div class="form-row">
                                                                    <div class="col-md-4 mb-3">
                                                                        <label for="heading_${heading_index}_field_${field_index}_value">Value</label>
                                                                        <input type="text" class="form-control" id="heading_${heading_index}_field_${field_index}_option_1_value" name="heading[${heading_index}][field][${field_index}][option][1][value]" placeholder="2">
                                                                    </div>
                                                                    <div class="col-md-4 mb-3">
                                                                        <label for="heading_${heading_index}_field_${field_index}_name">Name</label>
                                                                        <input type="text" class="form-control" id="heading_${heading_index}_field_${field_index}_option_1_name" name="heading[${heading_index}][field][${field_index}][option][1][name]" placeholder="2">
                                                                    </div>
                                                                    <div class="col-md-4 mb-3">
                                                                        <label for="heading_${heading_index}_field_${field_index}_unit">Unit</label>
                                                                        <input type="text" class="form-control" id="heading_${heading_index}_field_${field_index}_option_1_unit" name="heading[${heading_index}][field][${field_index}][option][1][unit]" placeholder="GB">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-1">
                                                        <div class="d-block bg-light">
                                                        	<button id="addOptions_1_heading_${heading_index}_field_${field_index}" heading_index="${heading_index}" field_index="${field_index}" option_index="1" title="Add more Field" class="btn btn-primary float-right rounded-circle">
                                                                <i class="fa fa-plus" aria-hidden="true"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="d-block bg-light">
                                    <button id="addField_${field_index}_heading_${heading_index}" heading_index="${heading_index}" field_index="${field_index}" title="Add More Field" class="btn btn-primary float-right rounded-circle">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <br/>
                                <br/>
                                <div class="d-block bg-light">
                                    <button id="deleteField_${field_index}_heading_${heading_index}" heading_index="${heading_index}" field_index="${field_index}" title="Delete this Section" class="btn btn-danger float-right rounded-circle">
                                        <i class="fa fa-minus" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>`;

        jQuery("#addField_" + (field_index - 1) + '_heading_' + heading_index).hide();
        jQuery("#deleteField_" + (field_index - 1) + '_heading_' + heading_index).hide();
        jQuery(this).closest('.fieldsWrapper').append(template);
	}

	function handleAddOption(event) {
		event.preventDefault();

		let recentIndexElement = jQuery(this).closest('.selectWrapper .row').last().find("[id^=addOptions_]");

		let heading_index = recentIndexElement.attr("heading_index");
		let field_index = recentIndexElement.attr("field_index");
		let option_index = parseInt(recentIndexElement.attr("option_index")) + 1;
		let template = `<div class="row mb-3">
                            <div class="col-11">
                                <div class="card">
                                    <div class="card-body">
                                        <div class="form-row">
                                            <div class="col-md-4 mb-3">
                                                <label for="heading_${heading_index}_field_${field_index}_option_${option_index}_value">Value</label>
                                                <input type="text" class="form-control" id="heading_${heading_index}_field_${field_index}_option_${option_index}_value" name="heading[${heading_index}][field][${field_index}][option][${option_index}][value]" placeholder="2">
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="heading_${heading_index}_field_${field_index}_option_${option_index}_name">Name</label>
                                                <input type="text" class="form-control" id="heading_${heading_index}_field_${field_index}_option_${option_index}_name" name="heading[${heading_index}][field][${field_index}][option][${option_index}][name]" placeholder="2">
                                            </div>
                                            <div class="col-md-4 mb-3">
                                                <label for="heading_${heading_index}_field_${field_index}_option_${option_index}_unit">Unit</label>
                                                <input type="text" class="form-control" id="heading_${heading_index}_field_${field_index}_option_${option_index}_unit" name="heading[${heading_index}][field][${field_index}][option][${option_index}][unit]" placeholder="GB">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-1">
                                <div class="d-block bg-light">
                                    <button id="addOptions_${option_index}_heading_${heading_index}_field_${field_index}" heading_index="${heading_index}" field_index="${field_index}" option_index="${option_index}" title="Add More Possible Value for Select Dropdown" class="btn btn-primary float-right rounded-circle">
                                        <i class="fa fa-plus" aria-hidden="true"></i>
                                    </button>
                                </div>
                                <br/>
                                <br/>
                                <div class="d-block bg-light">
                                    <button id="deleteOptions_${option_index}_heading_${heading_index}_field_${field_index}" heading_index="${heading_index}" field_index="${field_index}" option_index="${option_index}" title="Delete this section" class="btn btn-danger float-right rounded-circle">
	                                    <i class="fa fa-minus" aria-hidden="true"></i>
                                    </button>
                                </div>
                            </div>
                        </div>`;


        jQuery("#addOptions_" + (option_index - 1) + '_heading_' + heading_index + '_field_' + field_index).hide();
        jQuery("#deleteOptions_" + (option_index - 1) + '_heading_' + heading_index + '_field_' + field_index).hide();
        jQuery(this).closest('.selectWrapper').append(template);
	}

	function handleDeleteOption(event) {
		event.preventDefault();

		let heading_index = jQuery(this).attr("heading_index");
		let field_index = jQuery(this).attr("field_index");
		let option_index = parseInt(jQuery(this).attr("option_index"));

		jQuery("#addOptions_" + (option_index - 1) + '_heading_' + heading_index + '_field_' + field_index).show();
        jQuery("#deleteOptions_" + (option_index - 1) + '_heading_' + heading_index + '_field_' + field_index).show();
		jQuery(this).closest('.row .mb-3').remove();
	}

	function handleDeleteField(event) {
		event.preventDefault();

		let heading_index = jQuery(this).attr("heading_index");
		let field_index = jQuery(this).attr("field_index");

		jQuery("#addField_" + (field_index - 1) + '_heading_' + heading_index).show();
        jQuery("#deleteField_" + (field_index - 1) + '_heading_' + heading_index).show();
		jQuery(this).closest('.row .mb-3').remove();
	}

	function handleDeleteHeading(event) {
		event.preventDefault();

		let heading_index = jQuery(this).attr("heading_index");

		jQuery("#addHeading_" + (heading_index - 1)).show();
        jQuery("#deleteHeading_" + (heading_index - 1)).show();
		jQuery(this).closest('.row .mb-3').remove();
	}

	function fieldTypeSelectHandler(event) {
        let selectedType = this.value;

        if(!selectedType) {
            alert("Please Select Type");
        }

        switch(selectedType.toLowerCase()) {
            case 'select':
                _handleSelect(this, event);
                break;

            case 'text' :
            	_handleText(this, event)
                break;

            /*case 'number' :
                break;*/
        }
    }

    function _handleSelect(that, event) {
        jQuery(that).closest('.form-group').find(".selectWrapper").show();
    }

    function _handleText(that, event) {
    	jQuery(that).closest('.form-group').find(".selectWrapper").hide();
    }


	function handleCategoryUpdation(event) {
		event.preventDefault();
		
		return Promise.resolve(true)
			.then(() => {
				let cat_db = new CategoryDB();
				let form_data_json = convertFormDataToObject(jQuery('#updateCategoryForm'));

				let final_data = _format_json(form_data_json);

				return cat_db.updateCategory(category_id, final_data);
			})
			.then((results) => {
				let utilityObj = new Utility();

				alert("Category Saved Successfully.");

				utilityObj.redirect('categories/listing.html');

				return true;
			})
	}

	function _format_json(form_data_json) {
		let final_data = {};

		for(let key in form_data_json)
		{
			if(key == "heading")
			{
				final_data[key] = {};

				for(let _heading_index in form_data_json[key])
				{
					let heading_name = form_data_json[key][_heading_index]["name"];
					final_data[key][heading_name] = {};

					for(let _heading_key in form_data_json[key][_heading_index])
					{
						if(_heading_key == "field")
						{
							final_data[key][heading_name][_heading_key] = {};

							for(let _field_index in form_data_json[key][_heading_index][_heading_key])
							{
								final_data[key][heading_name][_heading_key][_field_index] = {};
								
								if(form_data_json[key][_heading_index][_heading_key][_field_index]['type'] == "text")
								{
									final_data[key][heading_name][_heading_key][_field_index]['name'] = form_data_json[key][_heading_index][_heading_key][_field_index]['name'];
									final_data[key][heading_name][_heading_key][_field_index]['type'] = form_data_json[key][_heading_index][_heading_key][_field_index]['type'];
									final_data[key][heading_name][_heading_key][_field_index]['possible_values'] = "";
								}

								if(form_data_json[key][_heading_index][_heading_key][_field_index]['type'] == "select") 
								{
									final_data[key][heading_name][_heading_key][_field_index]['name'] = form_data_json[key][_heading_index][_heading_key][_field_index]['name'];
									final_data[key][heading_name][_heading_key][_field_index]['type'] = form_data_json[key][_heading_index][_heading_key][_field_index]['type'];

									final_data[key][heading_name][_heading_key][_field_index]['option'] = {};

									for(let _option_key in form_data_json[key][_heading_index][_heading_key][_field_index]['option'])
									{

										if(form_data_json[key][_heading_index][_heading_key][_field_index]['option'][_option_key]['value']
											&& form_data_json[key][_heading_index][_heading_key][_field_index]['option'][_option_key]['name'])
										{
											final_data[key][heading_name][_heading_key][_field_index]['option'][_option_key] = {};

											final_data[key][heading_name][_heading_key][_field_index]['option'][_option_key]['value'] =  form_data_json[key][_heading_index][_heading_key][_field_index]['option'][_option_key]['value'];
											final_data[key][heading_name][_heading_key][_field_index]['option'][_option_key]['name'] =  form_data_json[key][_heading_index][_heading_key][_field_index]['option'][_option_key]['name'];
											final_data[key][heading_name][_heading_key][_field_index]['option'][_option_key]['unit'] =  (form_data_json[key][_heading_index][_heading_key][_field_index]['option'][_option_key]['unit']) ? form_data_json[key][_heading_index][_heading_key][_field_index]['option'][_option_key]['unit'] : "";
										}
									}
								}
							}

							continue;
						}

						final_data[key][heading_name][_heading_key] = form_data_json[key][_heading_index][_heading_key];
					}						
				}

				continue;
			}

			final_data[key] = form_data_json[key];
		}

		return final_data;
	}

	jQuery("#adminLogin").on('click', handleLogin);

	jQuery(document).on('click', '[id^=addHeading_]', handleAddHeading);
	jQuery(document).on('click', '[id^=addField_]', handleAddField);
	jQuery(document).on('click', '[id^=addOptions_]', handleAddOption);

	jQuery(document).on('click', '[id^=deleteOptions_]', handleDeleteOption);
	jQuery(document).on('click', '[id^=deleteField_]', handleDeleteField);
	jQuery(document).on('click', '[id^=deleteHeading_]', handleDeleteHeading);

    jQuery(document).on('change', '.select_field_type', fieldTypeSelectHandler);

	jQuery(document).on('click', "#updateCategory", handleCategoryUpdation);
})